from threading import Thread
from flask_mail import Mail, Message
from flask import render_template
from . import mail, create_app


def send_email_async(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(to, subject, template, **kwargs):
    msg = Message(create_app.config['FLASK_MAIL_SUBJECT_PREFIX'] + subject,
                  sender=create_app.config['MAIL_USERNAME'], recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_email_async, args=[app, msg])
    thr.start()
    return thr